const fs = require('fs')
const path = require('path')

/** File system */
/** @fs docs: https://nodejs.org/api/fs.html */

const createFolder = folderName => {
  const folderPath = path.join(__dirname, folderName)

  fs.access(folderPath, accessErr => {
    if (accessErr) {
      console.log(`${folderName} does not exist`)

      fs.mkdir(folderPath, mkdirErr => {
        if (mkdirErr) {
          throw mkdirErr
        }
        console.log('Folder was created')
      })
    }
  })
}

const createTextFile = (filename, content) => {
  fs.writeFile(path.join(__dirname, 'notes', `${filename}.txt`), content, err => {
    if (err) {
      throw err
    }
    console.log('File was created')
  })
}


const appendFile = (file, content) => {
  fs.appendFile(file, content, err => {
    if (err) {
      throw err
    }
    console.log('File was updated')
  })
}


const readFile = file => {
  fs.readFile(file, 'utf8', (err, data) => {
    if (err) {
      throw err
    }
    // If no charset Buffer can be used here
    // console.log(Buffer.from(data).toString())
    console.log(data)
  })
}


const renameFile = (oldName, newName) => {
  const oldPath = path.join(__dirname, 'notes', oldName)
  const newPath = path.join(__dirname, 'notes', newName)
  fs.rename(oldPath, newPath, err => {
    if (err) {
      throw err
    }
    console.log(`The file ${oldName} was renamed to ${newName}.`)
  })
}

createFolder('notes')
createTextFile('my_notes', 'Hello world')
appendFile(path.join(__dirname, 'notes', 'my_notes.txt'), '. A new content was added here...')
readFile(path.join(__dirname, 'notes', 'my_notes.txt'))
renameFile('my_notes.txt', 'notes.txt')

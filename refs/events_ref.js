const EventEmitter = require('events')

const getDate = () => {
  const options = {
    year: 'numeric',
    month: 'numeric',
    day: 'numeric',
    hour: 'numeric',
    minute: 'numeric',
    second: 'numeric'
  }

  return new Date().toLocaleDateString('ru-ru', options)
}

class Logger extends EventEmitter {
  log (message) {
    this.emit('message', `[${getDate()}]: ${message}`)
  }
}

const logger = new Logger()
logger.on('message', console.log)

logger.log('Hello')
logger.log('Hello')
logger.log('Hello')
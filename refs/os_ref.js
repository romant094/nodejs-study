const os = require('os')

// Operational system

// Get operational system
console.log(os.platform())

// Get architecture
console.log(os.arch())

// Get info about CPU
console.log(os.cpus())

// Get free memory amount
console.log(os.freemem())

// Get total memory amount
console.log(os.totalmem())

// Get home user directory
console.log(os.homedir())

// Get info how much time does the computer has been working in ms
console.log(os.uptime())
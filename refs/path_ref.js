const path = require('path')

/** @path docs: https://nodejs.org/api/path.html */
// console.log(path.basename(__filename))
// console.log(path.dirname(__filename))
// console.log(path.extname(__filename))
// console.log(path.parse(__filename))
console.log(path.join(__dirname, 'test', 'text-file.txt'))
console.log(path.resolve(__dirname, './test', '/text-file.txt'))
function consoleToJSON () {
  const cliParamsObject = {}

  for (let i = 2; i < process.argv.length; i++) {
    const [key, value] = process.argv[i].split('=')
    cliParamsObject[key] = value === 'false' ? false : value || true
  }

  return cliParamsObject
}

console.log(consoleToJSON())
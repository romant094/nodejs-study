const http = require('http')
const fs = require('fs')
const path = require('path')

const port = 3000

const sendContentFromHtml = (res, view) => {
  fs.readFile(
    path.join(__dirname, 'views', view),
    'utf-8',
    (err, content) => {
      if (err) {
        throw err
      }
      res.end(content)
    }
  )
}

const server = http.createServer((req, res) => {
  if (req.method === 'GET') {
    res.writeHead(200, {
      'Content-Type': 'text/html; charset=utf-8'
    })

    switch (req.url) {
      case '/': {
        sendContentFromHtml(res, 'index.html')
        break
      }
      case '/about': {
        sendContentFromHtml(res, 'about.html')
        break
      }
      case '/api/users': {
        res.writeHead(200, {
          'Content-Type': 'text/json'
        })
        const users = [
          { name: 'John' },
          { name: 'Helen' }
        ]
        res.end(JSON.stringify(users))
        break
      }
      default:
        res.writeHead(302, {
          Location: '/'
        })
        res.end()
    }
  } else if (req.method === 'POST') {
    res.writeHead(200, {
      'Content-Type': 'text/html; charset=utf-8'
    })

    const body = []

    req.on('data', data => {
      body.push(Buffer.from(data))
    })

    req.on('end', () => {
      const message = body.toString().split('=')[1]

      res.end(`Your message: ${message}`)
    })
  }
})

server.listen(port, () => {
  console.log(`Server is running on port ${port}.`)
})